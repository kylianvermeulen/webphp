@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="m-2 mb-4">Progressie page</h1>
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                @foreach($semester_ids as $semester_id)
                                    <a class="nav-item nav-link act {{$semester_id ===  1 ? 'active' : ''}}"
                                       id="nav-tab-{{$semester_id}}"
                                       data-toggle="tab" href="#nav-profile-{{$semester_id}}" role="tab"
                                       aria-controls="nav-profile-{{$semester_id}}"
                                       aria-selected="false">Semester {{$semester_id}}
                                    </a>
                                @endforeach
                                @foreach($module_ids as $module_id)
                                    <a class="nav-item nav-link act" id="nav-tab-{{$module_id + $total_semesters}}"
                                       data-toggle="tab" href="#nav-profile-{{$module_id + $total_semesters}}"
                                       role="tab"
                                       aria-controls="nav-profile-{{$module_id + $total_semesters}}"
                                       aria-selected="false">Blok {{$module_id}}
                                    </a>
                                @endforeach
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            @foreach($semester_ids as $semester_id)
                                <div class="tab-pane fade show" id="nav-profile-{{$semester_id}}"
                                     aria-labelledby="nav-tab-{{$semester_id}}">
                                    <table class="table" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Recieved / Total EC</th>
                                            <th>Grade</th>
                                        </tr>
                                        </thead>
                                        @php
                                            $amount_of_subjects = 0;
                                            $amount_of_completed_subjects = 0
                                        @endphp
                                        @foreach($modules as $module)
                                            @if(ceil(($module->period / 2)) == $semester_id)
                                                @php
                                                    $amount_of_subjects++
                                                @endphp
                                                <tbody>
                                                <tr>
                                                    <td>{{$module->name}}</td>

                                                    @if($module->completed)
                                                        @php
                                                            $amount_of_completed_subjects++
                                                        @endphp
                                                        <td>{{$module->ec}}/{{$module->ec}}</td>
                                                    @else
                                                        <td>0/{{$module->ec}}</td>
                                                    @endif

                                                    @if($module->score == null)
                                                        <td>Not graded</td>
                                                    @else
                                                        <td>{{$module->score}}</td>
                                                    @endif
                                                </tr>
                                                </tbody>
                                            @endif
                                        @endforeach
                                    </table>
                                    <div class="progress">
                                        @if($amount_of_completed_subjects === 0 )
                                            <div class="progress-bar progress-bar-striped" role="progressbar"
                                                 style="width: 3%;" aria-valuenow="0" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                0%
                                            </div>
                                        @else
                                            <div class="progress-bar progress-bar-striped" role="progressbar"
                                                 style="width: {{ceil($amount_of_completed_subjects / $amount_of_subjects * 100)}}%;"
                                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                {{ceil($amount_of_completed_subjects / $amount_of_subjects * 100)}}%
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            @foreach($module_ids as $module_id)
                                <div class="tab-pane fade show" id="nav-profile-{{$module_id + $total_semesters}}"
                                     aria-labelledby="nav-tab-{{$module_id + $total_semesters}}">
                                    <table class="table" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Recieved / Total EC</th>
                                            <th>Grade</th>
                                        </tr>
                                        </thead>
                                        @php
                                            $amount_of_subjects = 0;
                                            $amount_of_completed_subjects = 0
                                        @endphp
                                        @foreach($modules as $module)
                                            @if($module->period === $module_id)
                                                @php
                                                    $amount_of_subjects++
                                                @endphp
                                                <tbody>
                                                <tr>
                                                    <td>{{$module->name}}</td>
                                                    @if($module->completed)
                                                        @php
                                                            $amount_of_completed_subjects++
                                                        @endphp
                                                        <td>{{ $module->ec }}/{{ $module->ec }}</td>
                                                    @else
                                                        <td>0/{{$module->ec}}</td>
                                                    @endif

                                                    @if($module->score == null)
                                                        <td>Not graded</td>
                                                    @else
                                                        <td>{{$module->score}}</td>
                                                    @endif
                                                </tr>
                                                </tbody>
                                            @endif
                                        @endforeach
                                    </table>
                                    <div class="progress">
                                        @if($amount_of_completed_subjects === 0 )
                                            <div class="progress-bar progress-bar-striped" role="progressbar"
                                                 style="width: 3%;" aria-valuenow="0" aria-valuemin="0"
                                                 aria-valuemax="100">
                                                0%
                                            </div>
                                        @else
                                            <div class="progress-bar progress-bar-striped" role="progressbar"
                                                 style="width: {{ceil($amount_of_completed_subjects / $amount_of_subjects * 100)}}%;"
                                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                {{ceil($amount_of_completed_subjects / $amount_of_subjects * 100)}}%
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
