@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="vacancy-list-header col-md-12 d-flex justify-content-center align-items-center">
                <form action="{{ route('admin') }}" method="GET">
                    <div class="search-form d-flex flex-row">
                        @if($filter === 'modules')
                            <a class="search-create search-form-shadow-group d-flex align-items-center justify-content-center"
                               href="{{ route('modules.create') }}">
                                <i class="fas fa-plus text-white create"></i>
                            </a>
                            <div class="search-form-shadow-group ml-2 d-flex flex-row">
                                <input class="form-control w-100" id="search" name="search"
                                       placeholder="Zoeken naar module(s)"
                                       type="text" value="{{ $search }}">
                                <input type="hidden" name="filter" value="{{ $filter }}">
                                <input type="hidden" name="page" value="{{ $currentPage }}">
                                <button class="search-submit d-flex align-items-center justify-content-center"
                                        type="submit">
                                    <i class="fas fa-search text-white"></i>
                                </button>
                            </div>
                        @elseif($filter === 'teachers')
                            <a class="search-create search-form-shadow-group d-flex align-items-center justify-content-center"
                               href="{{ route('teachers.create') }}">
                                <i class="fas fa-plus text-white create"></i>
                            </a>
                            <div class="search-form-shadow-group ml-2 d-flex flex-row">
                                <input class="form-control w-100" id="search" name="search"
                                       placeholder="Zoeken naar teacher(s)"
                                       type="text" value="{{ $search }}">
                                <input type="hidden" name="filter" value="{{ $filter }}">
                                <input type="hidden" name="page" value="{{ $currentPage }}">
                                <button class="search-submit d-flex align-items-center justify-content-center"
                                        type="submit">
                                    <i class="fas fa-search text-white"></i>
                                </button>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="vacancy-list-filter col-md-12 d-flex justify-content-center align-items-center">
                <a class="btn btn-link @if($filter === 'modules') btn-link-active @endif text-white mx-2"
                   href="{{ route('admin', ['filter' => 'modules', 'search' => $search, 'page' => $currentPage]) }}">Modules</a>
                <a class="btn btn-link @if($filter === 'teachers') btn-link-active @endif text-white mx-2"
                   href="{{ route('admin', ['filter' => 'teachers', 'search' => $search, 'page' => $currentPage]) }}">Teachers</a>
            </div>
        </div>
        @if($lastPage > 1)
            <div class="row fixed-bottom mb-4">
                <div class="col-md-12 d-flex flex-row justify-content-center align-items-center">
                    @for($i = 1; $i <= $lastPage; $i++)
                        <a class="pagination-item @if($currentPage == $i) pagination-item-active @endif d-flex align-items-center justify-content-center text-white mx-1"
                           href="{{ route('admin', ['filter' => $filter, 'search' => $search, 'page' => $i]) }}">
                            <label class="mb-0">{{ $i }}</label>
                        </a>
                    @endfor
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 p-0">
                <ul class="vacancy-list p-0 m-0">
                    <li class="hover">
                        <div class="row no-gutters">
                            <div class="col-md-12">
                                <div class="vacancy-count d-flex flex-row">
                                    <small>{{ $totalCount }} resultaten</small>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php
                    use App\Module;use App\Teacher;$i = 1;
                    for ($x = 1; $x < $currentPage; $x++) {
                        $i += 15;
                    }
                    ?>
                    @foreach($data as $item)
                        <li class="hover">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="vacancy d-flex justify-content-between flex-row">
                                        <div class="d-flex flex-row">
                                            <small>{{ $i }}</small>
                                            @if($filter === 'modules')
                                                <?php /** @var Module $item */ ?>
                                                <h5 class="pl-2">{{ $item->name }}</h5>
                                                <p class="m-0 pl-2">Period: {{ $item->period }}</p>
                                                <p class="m-0 pl-2">EC: {{ $item->ec }}</p>
                                                <p class="m-0 pl-2">Completed: {{ $item->completed ? 'ja' : 'nee' }}</p>
                                                <p class="m-0 pl-2">Score: {{ $item->score ?? 'NA' }}</p>
                                                <p class="m-0 pl-2">Type: {{ $item->type }}</p>
                                                <p class="m-0 pl-2">Teacher: {{ $item->teacher->name ?? '' }}</p>
                                                <p class="m-0 pl-2">Coordinator: {{ $item->coordinator->name ?? '' }}</p>
                                                    @if($item->file_path != null) <p class="m-0 pl-2"><a href="/storage/{{$item->file_path}}">File download</a></p> @endif
                                            @elseif($filter === 'teachers')
                                                <?php /** @var Teacher $item */ ?>
                                                <h5 class="pl-2">{{ $item->name }}</h5>
                                            @endif
                                        </div>
                                        <div class="d-flex flex-row">
                                            <p class="m-0 pl-2">{{ $item->updated_at }}</p>
                                            <div class="dropdown ml-2">
                                                <button type="button" class="btn btn-primary dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    <i class="fas fa-angle-up"></i>
                                                </button>
                                                <ul class="text-left dropdown-menu p-0">
                                                    @if($filter === 'modules')
                                                        <?php /** @var Module $item */ ?>
                                                        <a href="{{ route('modules.edit', $item->id) }}">Bewerken</a>
                                                        <a href="{{ route('modules.destroy', $item->id) }}"
                                                           onclick="event.preventDefault();
                                                               document.getElementById('delete-form-{{$item->id}}').submit();">Verwijderen</a>
                                                        <form id="delete-form-{{$item->id}}"
                                                              action="{{ route('modules.destroy', $item->id) }}"
                                                              method="POST">
                                                            @method('DELETE')
                                                            @csrf
                                                        </form>
                                                    @elseif($filter === 'teachers')
                                                        <?php /** @var Teacher $item */ ?>
                                                        <a href="{{ route('teachers.edit', $item->id) }}">Bewerken</a>
                                                        <a href="{{ route('teachers.destroy', $item->id) }}"
                                                           onclick="event.preventDefault();
                                                               document.getElementById('delete-form-{{$item->id}}').submit();">Verwijderen</a>
                                                        <form id="delete-form-{{$item->id}}"
                                                              action="{{ route('teachers.destroy', $item->id) }}"
                                                              method="POST">
                                                            @method('DELETE')
                                                            @csrf
                                                        </form>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php $i++ ?>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
