@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <form action="{{ route('deadlines.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h2 class="text-primary">Deadline aanmaken</h2>
                            <p>Maak een deadline aan om belangrijke momenten in een module bij te houden.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="name" class="text-secondary">Naam / Omschrijving</label>
                            <input id="name" type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}"
                                   placeholder="Vul hier de naam/omschrijving van de deadine in">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="deadline_at" class="text-secondary">Deadline</label>
                            <input id="deadline_at" type="text"
                                   class="form-control @error('deadline_at') is-invalid @enderror"
                                   name="deadline_at" value="{{ old('deadline_at') }}"
                                   placeholder="Vul hier de deadline van de deadine in">
                            @error('deadline_at')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="module_id">Module</label>
                            <select id="module_id" name="module_id"
                                    class="custom-select @error('module_id') is-invalid @enderror">
                                @if(old('module_id') == null)
                                    <option value="" disabled selected hidden>Kies een module voor de deadline</option>
                                @endif
                                @foreach($modules as $module)
                                    @if (old('module_id') == $module->id)
                                        <option value="{{ $module->id }}" selected>{{ $module->name }}</option>
                                    @else
                                        <option value="{{ $module->id }}">{{ $module->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('module_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="difficulty_id">Difficulty</label>
                            <select id="difficulty_id" name="difficulty_id"
                                    class="custom-select @error('difficulty_id') is-invalid @enderror">
                                @if(old('difficulty_id') == null)
                                    <option value="" disabled selected hidden>Kies een difficulty voor de deadline</option>
                                @endif
                                @foreach($difficulty_tags as $difficulty_tag)
                                    @if (old('difficulty_id') == $difficulty_tag->id)
                                        <option value="{{ $difficulty_tag->id }}" selected>{{ $difficulty_tag->name }}</option>
                                    @else
                                        <option value="{{ $difficulty_tag->id }}">{{ $difficulty_tag->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('difficulty_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="time_commitment_id">Time commitment</label>
                            <select id="time_commitment_id" name="time_commitment_id"
                                    class="custom-select @error('time_commitment_id') is-invalid @enderror">
                                @if(old('time_commitment_id') == null)
                                    <option value="" disabled selected hidden>Kies een time commitment voor de deadline</option>
                                @endif
                                @foreach($time_commitment_tags as $time_commitment_tag)
                                    @if (old('time_commitment_id') == $time_commitment_tag->id)
                                        <option value="{{ $time_commitment_tag->id }}" selected>{{ $time_commitment_tag->name }}</option>
                                    @else
                                        <option value="{{ $time_commitment_tag->id }}">{{ $time_commitment_tag->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('time_commitment_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="fun_id">Fun</label>
                            <select id="fun_id" name="fun_id"
                                    class="custom-select @error('fun_id') is-invalid @enderror">
                                @if(old('fun_id') == null)
                                    <option value="" disabled selected hidden>Kies een fun voor de deadline</option>
                                @endif
                                @foreach($fun_tags as $fun_tag)
                                    @if (old('fun_id') == $fun_tag->id)
                                        <option value="{{ $fun_tag->id }}" selected>{{ $fun_tag->name }}</option>
                                    @else
                                        <option value="{{ $fun_tag->id }}">{{ $fun_tag->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('fun_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 d-flex justify-content-between">
                            <button type="submit" class="btn btn-primary">Aanmaken van deadline</button>
                            <a href="{{ route('deadlines.index') }}" class="text-secondary align-self-end">Annuleren</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
