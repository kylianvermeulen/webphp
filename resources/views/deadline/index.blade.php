@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="vacancy-list-header col-md-12 d-flex justify-content-center align-items-center">
                <form action="{{ route('deadlines.index') }}" method="GET">
                    <div class="search-form d-flex flex-row">
                        <a class="search-create search-form-shadow-group d-flex align-items-center justify-content-center"
                           href="{{ route('deadlines.create') }}">
                            <i class="fas fa-plus text-white create"></i>
                        </a>
                        <div class="search-form-shadow-group ml-2 d-flex flex-row">
                            <input class="form-control w-100" id="search" name="search"
                                   placeholder="Zoeken naar deadline(s)"
                                   type="text" value="{{ $search }}">
                            <input type="hidden" name="deadlineSort" value="{{ $deadlineSort }}">
                            <input type="hidden" name="deadlineSort" value="{{ $deadlineTimeSort }}">
                            <input type="hidden" name="deadlineSort" value="{{ $moduleSort }}">
                            <input type="hidden" name="deadlineSort" value="{{ $docentSort }}">
                            <input type="hidden" name="deadlineSort" value="{{ $difficultySort }}">
                            <input type="hidden" name="deadlineSort" value="{{ $timeCommitmentSort }}">
                            <input type="hidden" name="deadlineSort" value="{{ $funSort }}">
                            <button class="search-submit d-flex align-items-center justify-content-center"
                                    type="submit">
                                <i class="fas fa-search text-white"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if($lastPage > 1)
            <div class="row fixed-bottom mb-4">
                <div class="col-md-12 d-flex flex-row justify-content-center align-items-center">
                    @for($i = 1; $i <= $lastPage; $i++)
                        <a class="pagination-item @if($currentPage == $i) pagination-item-active @endif d-flex align-items-center justify-content-center text-white mx-1"
                           href="{{ route('deadlines.index', ['search' => $search, 'deadlineSort' => $deadlineSort, 'page' => $i]) }}">
                            <label class="mb-0">{{ $i }}</label>
                        </a>
                    @endfor
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 p-0">
                <ul class="vacancy-list p-0 m-0">
                    <li class="hover">
                        <div class="row no-gutters">
                            <div class="col-md-12">
                                <div class="vacancy-count d-flex flex-row">
                                    <small>{{ $totalCount }} resultaten</small>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php
                    use App\Deadline;
                    $i = 1;
                    for ($x = 1; $x < $currentPage; $x++) {
                        $i += 15;
                    }
                    ?>
                    @foreach($data as $item)
                        <li class="hover @if($item->completed) completed @endif">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="vacancy d-flex justify-content-between flex-row">
                                        <div class="d-flex flex-row">
                                            <small>{{ $i }}</small>
                                            <?php /** @var Deadline $item */ ?>
                                            <h5 class="pl-4">
                                                @if($deadlineSort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'deadlineSort' => 'asc']) }}" class="h5 text-decoration-none text-dark">{{ $item->name }}</a>
                                                @elseif($deadlineSort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'deadlineSort' => 'desc']) }}" class="h5 text-decoration-none text-dark">{{ $item->name }}</a>
                                                @elseif($deadlineSort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'deadlineSort' => 'none']) }}" class="h5 text-decoration-none text-dark">{{ $item->name }}</a>
                                                @endif
                                            </h5>
                                            <h5 class="pl-4">
                                                @if($deadlineTimeSort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'deadlineTimeSort' => 'asc']) }}" class="h5 text-decoration-none text-dark">{{ $item->deadline_at }}</a>
                                                @elseif($deadlineTimeSort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'deadlineTimeSort' => 'desc']) }}" class="h5 text-decoration-none text-dark">{{ $item->deadline_at }}</a>
                                                @elseif($deadlineTimeSort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'deadlineTimeSort' => 'none']) }}" class="h5 text-decoration-none text-dark">{{ $item->deadline_at }}</a>
                                                @endif
                                            </h5>
                                            <p class="m-0 pl-4">
                                                @if($moduleSort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'moduleSort' => 'asc']) }}" class="h5 text-decoration-none text-dark">Module: {{ $item->module->name }}</a>
                                                @elseif($moduleSort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'moduleSort' => 'desc']) }}" class="h5 text-decoration-none text-dark">Module: {{ $item->module->name }}</a>
                                                @elseif($moduleSort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'moduleSort' => 'none']) }}" class="h5 text-decoration-none text-dark">Module: {{ $item->module->name }}</a>
                                                @endif
                                            </p>
                                            <p class="m-0 pl-4">
                                                @if($docentSort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'docentSort' => 'asc']) }}" class="h5 text-decoration-none text-dark">Docent: {{ $item->module->teacher->name }}</a>
                                                @elseif($docentSort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'docentSort' => 'desc']) }}" class="h5 text-decoration-none text-dark">Docent: {{ $item->module->teacher->name }}</a>
                                                @elseif($docentSort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'docentSort' => 'none']) }}" class="h5 text-decoration-none text-dark">Docent: {{ $item->module->teacher->name }}</a>
                                                @endif
                                            </p>
                                            <p class="m-0 pl-4">
                                                @if($difficultySort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'difficultySort' => 'asc']) }}" class="h5 text-decoration-none text-dark">Difficulty: {{ $item->difficulty->name }}</a>
                                                @elseif($difficultySort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'difficultySort' => 'desc']) }}" class="h5 text-decoration-none text-dark">Difficulty: {{ $item->difficulty->name }}</a>
                                                @elseif($difficultySort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'difficultySort' => 'none']) }}" class="h5 text-decoration-none text-dark">Difficulty: {{ $item->difficulty->name }}</a>
                                                @endif
                                            </p>
                                            <p class="m-0 pl-4">
                                                @if($timeCommitmentSort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'timeCommitmentSort' => 'asc']) }}" class="h5 text-decoration-none text-dark">Time commitment: {{ $item->time_commitment->name }}</a>
                                                @elseif($timeCommitmentSort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'timeCommitmentSort' => 'desc']) }}" class="h5 text-decoration-none text-dark">Time commitment: {{ $item->time_commitment->name }}</a>
                                                @elseif($timeCommitmentSort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'timeCommitmentSort' => 'none']) }}" class="h5 text-decoration-none text-dark">Time commitment: {{ $item->time_commitment->name }}</a>
                                                @endif
                                            </p>
                                            <p class="m-0 pl-4">
                                                @if($funSort == 'none')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'funSort' => 'asc']) }}" class="h5 text-decoration-none text-dark">Fun: {{ $item->fun->name }}</a>
                                                @elseif($funSort == 'asc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'funSort' => 'desc']) }}" class="h5 text-decoration-none text-dark">Fun: {{ $item->fun->name }}</a>
                                                @elseif($funSort == 'desc')
                                                    <a href="{{ route('deadlines.index', ['search' => $search, 'funSort' => 'none']) }}" class="h5 text-decoration-none text-dark">Fun: {{ $item->fun->name }}</a>
                                                @endif
                                            </p>
                                        </div>
                                        <div class="d-flex flex-row">
                                            <p class="m-0 pl-2">{{ $item->updated_at }}</p>
                                            <div class="dropdown ml-2">
                                                <button type="button" class="btn btn-primary dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    <i class="fas fa-angle-up"></i>
                                                </button>
                                                <ul class="text-left dropdown-menu p-0">
                                                    <a href="{{ route('deadlines.edit', $item->id) }}">Bewerken</a>
                                                    <a href="{{ route('deadlines.complete', $item->id) }}">
                                                        @if($item->completed)
                                                            Niet gedaan
                                                        @else
                                                            Gedaan
                                                        @endif
                                                    </a>
                                                    <a href="{{ route('deadlines.destroy', $item->id) }}"
                                                       onclick="event.preventDefault();
                                                           document.getElementById('delete-form-{{$item->id}}').submit();">Verwijderen</a>
                                                    <form id="delete-form-{{$item->id}}"
                                                          action="{{ route('deadlines.destroy', $item->id) }}"
                                                          method="POST">
                                                        @method('DELETE')
                                                        @csrf
                                                    </form>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php $i++ ?>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
