@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <form action="{{ route('teachers.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h2 class="text-primary">Teacher aanmaken</h2>
                            <p>Maak een teacher aan om deze bij een module te kopppelen.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="name" class="text-secondary">Teacher naam</label>
                            <input id="name" type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') }}"
                                   placeholder="Vul hier de naam van de teacher in">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 d-flex justify-content-between">
                            <button type="submit" class="btn btn-primary">Aanmaken van teacher</button>
                            <a href="{{ route('admin') }}" class="text-secondary align-self-end">Annuleren</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
