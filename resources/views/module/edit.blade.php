@extends('layouts.default')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <form action="{{ route('modules.update', $module->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h2 class="text-primary">Module bewerken</h2>
                            <p>Bewerk een module aan om voortang voor deze module bij te houden.</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="name" class="text-secondary">Module naam</label>
                            <input id="name" type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name" value="{{ old('name') ?? $module->name }}"
                                   placeholder="Vul hier de naam van de module in">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="description" class="text-secondary">Omschrijving</label>
                            <input id="description" type="text"
                                   class="form-control @error('description') is-invalid @enderror"
                                   name="description" value="{{ old('description') ?? $module->description }}"
                                   autocomplete="description"
                                   placeholder="Vul hier de omschrijving van de module in">
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="period" class="text-secondary">Blok</label>
                            <input id="period" type="text"
                                   class="form-control @error('period') is-invalid @enderror"
                                   name="period" value="{{ old('period') ?? $module->period }}" autocomplete="period"
                                   placeholder="Vul hier de period van de module in">
                            @error('period')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="ec" class="text-secondary">EC</label>
                            <input id="ec" type="text"
                                   class="form-control @error('ec') is-invalid @enderror"
                                   name="ec" value="{{ old('ec') ?? $module->ec }}" autocomplete="ec"
                                   placeholder="Vul hier de ec van de module in">
                            @error('ec')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="completed" class="text-secondary">Completed <small>(niet
                                    verplicht)</small></label>
                            <input id="completed" type="text"
                                   class="form-control @error('completed') is-invalid @enderror"
                                   name="completed" value="{{ old('completed') ?? $module->completed ? 1 : 0 }}"
                                   autocomplete="completed"
                                   placeholder="Vul hier de status van de module in (0 of 1)">
                            @error('completed')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="score" class="text-secondary">Score <small>(niet verplicht)</small></label>
                            <input id="score" type="text"
                                   class="form-control @error('score') is-invalid @enderror"
                                   name="score" value="{{ old('score') ?? $module->score ?? '' }}" autocomplete="score"
                                   placeholder="Vul hier de score van de module in">
                            @error('score')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="type">Type</label>
                            <select id="type" name="type"
                                    class="custom-select @error('type') is-invalid @enderror">
                                @foreach($types as $type)
                                    @if (old('type') ?? $module->type == $type)
                                        <option value="{{ $type }}" selected>{{ $type }}</option>
                                    @else
                                        <option value="{{ $type }}">{{ $type }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="file_path" class="text-secondary">ZIP-bestand <small>(niet verplicht)</small></label>
                            <input type="file" class="form-control-file" id="file_path"
                                   name="file_path">
                            @error('file_path')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="type">Teacher</label>
                            <select id="teacher_id" name="teacher_id"
                                    class="custom-select @error('teacher_id') is-invalid @enderror">
                                @if(old('teacher_id') ?? $module->teacher_id == null)
                                    <option value="" disabled selected hidden>Kies een coordinator voor de module
                                    </option>
                                @endif
                                @foreach($teachers as $teacher)
                                    @if (old('teacher_id') ?? $module->teacher->id ?? '' == $teacher->id)
                                        <option value="{{ $teacher->id }}" selected>{{ $teacher->name }}</option>
                                    @else
                                        <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('teacher_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="text-secondary" for="type">Coordinator</label>
                            <select id="coordinator_id" name="coordinator_id"
                                    class="custom-select @error('coordinator_id') is-invalid @enderror">
                                @if(old('coordinator_id') ?? $module->coordinator_id == null)
                                    <option value="" disabled selected hidden>Kies een coordinator voor de module
                                    </option>
                                @endif
                                @foreach($teachers as $teacher)
                                    @if (old('coordinator_id') ?? $module->coordinator->id ?? '' == $teacher->id)
                                        <option value="{{ $teacher->id }}" selected>{{ $teacher->name }}</option>
                                    @else
                                        <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('coordinator_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 d-flex justify-content-between">
                            <button type="submit" class="btn btn-primary">Bewerken van module</button>
                            <a href="{{ route('admin') }}" class="text-secondary align-self-end">Annuleren</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
