@extends('layouts.app')

@section('page')
    <div class="site view-dashboard">
        @include('layouts.partials.navigation')
        <main>
            @yield('content')
        </main>
    </div>
@endsection
