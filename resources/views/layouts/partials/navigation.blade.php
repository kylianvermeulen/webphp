<nav class="navbar navbar-light navbar-expand-md bg-white">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('images/logo.png') }}" height="50px" alt="studymate"/>
        </a>
        <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggler">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav flex-grow-1 justify-content-end">
                @guest
                    <li role="presentation" class="nav-item">
                        <a class="nav-link text-primary" href="{{ route('login') }}">Login</a>
                    </li>
                @else
                    @if(auth()->user()->type === 'admin')
                        <li role="presentation" class="nav-item d-flex align-items-center mr-3">
                            <a class="nav-link text-primary" href="{{ route('admin') }}">Admin</a>
                        </li>
                    @elseif(auth()->user()->type === 'deadline')
                        <li role="presentation" class="nav-item d-flex align-items-center mr-3">
                            <a class="nav-link text-primary" href="{{ route('deadlines.index') }}">Deadline</a>
                        </li>
                    @endif
                    <li role="presentation" class="nav-item dropdown top-navigation">
                        <a class="nav-link dropdown-toggle text-primary" href="#" id="navbardrop"
                           data-toggle="dropdown">{{ auth()->user()->name }}</a>
                        <div class="dropdown-menu text-left">
                            <a class="can-hover" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Uitloggen
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
