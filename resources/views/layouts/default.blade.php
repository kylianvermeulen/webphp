@extends('layouts.app')

@section('page')
    <div class="site view-default">
        @include('layouts.partials.navigation')
        <main class="py-4">
            @yield('content')
        </main>
    </div>
@endsection
