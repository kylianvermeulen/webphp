<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'SiteController@actionIndex')->name('/');

Auth::routes(['register' => false]);

Route::get('/admin', 'SiteController@actionAdmin')->name('admin');

Route::resource('users', 'UserController');

Route::resource('modules', 'ModuleController');

Route::resource('teachers', 'TeacherController');

Route::resource('deadlines', 'DeadlineController');
Route::get('/deadlines/{deadline}/complete', 'DeadlineController@actionComplete')->name('deadlines.complete');

Route::resource('tags', 'TagController');
