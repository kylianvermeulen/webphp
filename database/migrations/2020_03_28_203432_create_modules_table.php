<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('teacher_id')->nullable();
            $table->unsignedBigInteger('coordinator_id')->nullable();
            $table->string('name');
            $table->string('description');
            $table->integer('period');
            $table->integer('ec');
            $table->boolean('completed')->default(false);
            $table->integer('score')->nullable();
            $table->string('type');
            $table->string('file_path')->nullable();
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('set null');
            $table->foreign('coordinator_id')->references('id')->on('teachers')->onDelete('set null');;
            $table->index('teacher_id');
            $table->index('coordinator_id');
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
