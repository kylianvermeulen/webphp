<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeadlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deadlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('module_id');
            $table->string('name');
            $table->timestamp('deadline_at');
            $table->boolean('completed')->default(false);
            $table->unsignedBigInteger('difficulty_id')->nullable();
            $table->unsignedBigInteger('time_commitment_id')->nullable();
            $table->unsignedBigInteger('fun_id')->nullable();
            $table->timestamps();

            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
            $table->foreign('difficulty_id')->references('id')->on('tags')->onDelete('set null');
            $table->foreign('time_commitment_id')->references('id')->on('tags')->onDelete('set null');
            $table->foreign('fun_id')->references('id')->on('tags')->onDelete('set null');
            $table->index('module_id');
            $table->index('difficulty_id');
            $table->index('time_commitment_id');
            $table->index('fun_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deadlines');
    }
}
