<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'name' => 'makkelijk',
            'type' => 'difficulty',
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);

        DB::table('tags')->insert([
            'name' => 'moeilijk',
            'type' => 'difficulty',
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);

        DB::table('tags')->insert([
            'name' => 'veel werk',
            'type' => 'time_commitment',
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);

        DB::table('tags')->insert([
            'name' => 'weinig werk',
            'type' => 'time_commitment',
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);
        DB::table('tags')->insert([
            'name' => 'leuk',
            'type' => 'fun',
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);

        DB::table('tags')->insert([
            'name' => 'niet leuk',
            'type' => 'fun',
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);
    }
}
