<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Crypt::encrypt('Kylian Vermeulen'),
            'email' => 'email@kylianvermeulen.com',
            'password' => bcrypt('password'),
            'type' => Crypt::encrypt('admin'),
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);

        DB::table('users')->insert([
            'name' => Crypt::encrypt('Jelles Duin'),
            'email' => 'jelles.duin@gmail.com',
            'password' => bcrypt('password'),
            'type' => Crypt::encrypt('deadline'),
            'created_at' => '2020-03-28 23:59:52',
            'updated_at' => '2020-03-28 23:59:52',
        ]);
    }
}
