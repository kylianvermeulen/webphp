<?php

/** @var Factory $factory */

use App\Module;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Module::class, function (Faker $faker) {
    $name = $faker->firstNameMale;
    $grade = rand(4, 10);
    if(rand(0,2) == 1)
        if($grade > 5)
            $completed = 1;
        else
            $completed = 0;
    else {
        $grade = null;
        $completed = 0;
    }
    if(rand(0,1) == 1)
        $type = 'assesment';
    else
        $type = 'tentamen';

    return [
        'teacher_id' => rand(1, 15),
        'coordinator_id' => rand(1, 15),
        'name' => $name,
        'description' => $name,
        'period' => rand(1, 16),
        'ec' => rand(1, 8),
        'completed' => $completed,
        'score' => $grade,
        'type' => $type,
    ];
});
