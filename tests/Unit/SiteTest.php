<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SiteTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function no_access_to_admin_without_auth()
    {
        $response = $this->get('/admin')->assertRedirect();
    }

    /** @test */
    public function access_to_admin_with_auth_and_admin_type()
    {
        $this->actingAs(factory(User::class)->create(['type' => 'admin']));
        $response = $this->get('/admin')->assertOk();
    }

    /** @test */
    public function no_access_to_admin_with_auth_and_deadline_type()
    {
        $this->actingAs(factory(User::class)->create(['type' => 'deadline']));
        $response = $this->get('/admin')->assertRedirect();
    }

    /** @test */
    public function access_to_index_without_auth()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /** @test */
    public function access_to_index_with_auth()
    {
        $this->actingAs(factory(User::class)->create(['type' => 'admin']));
        $response = $this->get('/')->assertOk();
    }
}
