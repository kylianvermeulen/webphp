<?php

namespace Tests\Unit;

use App\Module;
use App\Teacher;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ModuleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_with_type_admin()
    {
        $this->actingAs(factory(User::class)->create(['type' => 'admin']));
        $response = $this->get('/modules/create')->assertOk();
    }

    /** @test */
    public function can_not_create_with_type_deadline()
    {
        $this->actingAs(factory(User::class)->create(['type' => 'deadline']));
        $response = $this->get('/modules/create')->assertForbidden();
    }

    /** @test */
    public function can_post_with_type_admin()
    {
        $this->withoutMiddleware();
        $this->actingAs(factory(User::class)->create(['type' => 'admin']));
        $response = $this->post('/modules', [
            'name' => 'webphp',
            'description' => 'webphp',
            'period' => '10',
            'ec' => '3',
            'type' => 'assessment',
            'teacher_id' => factory(Teacher::class)->create()->id,
            'coordinator_id' => factory(Teacher::class)->create()->id,
        ]);
        $this->assertCount(1, Module::all());
    }

    /** @test */
    public function can_not_post_with_type_deadline()
    {
        $this->withoutMiddleware();
        $this->actingAs(factory(User::class)->create(['type' => 'deadline']));
        $response = $this->post('/modules', [
            'name' => 'webphp',
            'description' => 'webphp',
            'period' => '10',
            'ec' => '3',
            'type' => 'assessment',
            'teacher_id' => factory(Teacher::class)->create()->id,
            'coordinator_id' => factory(Teacher::class)->create()->id,
        ]);
        $this->assertCount(0, Module::all());
    }
}
