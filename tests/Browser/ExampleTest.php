<?php

namespace Tests\Browser;

use App\Module;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('Login');
        });
    }

    public function testLoginIncorrect()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('/')->click('#navcol-1 > ul > li > a');
            $browser->assertSee('E-Mail Address');
            $browser->assertSee('Password');
            $browser->type('email', 'jelles.duin@gmail.com');
            $browser->type('password', 'passwordd');
            $browser->click('#app > div > main > div > div > div > div > div.card-body > form > div.form-group.row.mb-0 > div > button');
            $browser->assertSee('These credentials do not match our records.');
        });
    }

    public function testLoginCorrect()
    {
        $this->browse(function (Browser $browser) {
           $browser->visitRoute('/')->click('#navcol-1 > ul > li > a');
           $browser->assertSee('E-Mail Address');
           $browser->assertSee('Password');
           $browser->type('email', 'jelles.duin@gmail.com');
           $browser->type('password', 'password');
           $browser->click('#app > div > main > div > div > div > div > div.card-body > form > div.form-group.row.mb-0 > div > button');
           $browser->assertSee('Deadline');
        });
    }

    public function testSearch()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('deadlines.index');
            $browser->type('search', 'Study');
            $browser->click('#app > div > main > div > div:nth-child(1) > div > form > div > div > button');
            $browser->assertSee('1 resultaten');
        });
    }

    public function testCreate()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('deadlines.index');
            $browser->click('#app > div > main > div > div:nth-child(1) > div > form > div > a');
            $browser->assertSee('Deadline aanmaken');
            $browser->value('#name', 'DUSK');
            $browser->value('#deadline_at', '2020-04-01 08:00:00');
            $browser->value('#module_id', '1');
            $browser->value('#difficulty_id', '1');
            $browser->value('#time_commitment_id', '3');
            $browser->value('#fun_id', '5');
            $browser->click('#app > div > main > div > div > div > form > div.form-group.row.mb-0 > div > button');
            $browser->assertSee('DUSK');
        });
    }

    public function testSortDeadlineAtAndDelete()
    {
        $this->browse(function (Browser $browser) {
            $browser->visitRoute('deadlines.index');
            $browser->click('#app > div > main > div > div:nth-child(2) > div > ul > li:nth-child(2) > div > div > div > div:nth-child(1) > h5:nth-child(2) > a');
            $browser->click('#app > div > main > div > div:nth-child(2) > div > ul > li:nth-child(2) > div > div > div > div:nth-child(1) > h5:nth-child(2) > a');
            $browser->assertSee('DUSK');
            $browser->click('#app > div > main > div > div:nth-child(2) > div > ul > li:nth-child(4) > div > div > div > div:nth-child(2) > div > button');
            $browser->click('#app > div > main > div > div:nth-child(2) > div > ul > li:nth-child(4) > div > div > div > div:nth-child(2) > div > ul > a:nth-child(3)');
            $browser->assertSee('2 resultaten');
            $browser->assertDontSee('DUSK');
        });
    }
}
