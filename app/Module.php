<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Module
 * @package App
 * @property int id
 * @property int teacher_id
 * @property int coordinator_id
 * @property string name
 * @property string description
 * @property int period
 * @property int ec
 * @property boolean completed
 * @property int score
 * @property string type
 * @property string deadline
 * @property string file_path
 * @property string created_at
 * @property string updated_at
 *
 * @property Teacher teacher
 * @property Teacher coordinator
 * @property Deadline[] deadlines
 */
class Module extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'period',
        'ec',
        'completed',
        'score',
        'type',
        'teacher_id',
        'coordinator_id',
        'file_path',
    ];

    public static function getModuleIds()
    {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    }

    public static function getSemesterIds()
    {
        return [1, 2, 3, 4, 5, 6, 7, 8];
    }

    protected $casts = [
        'completed' => 'boolean',
    ];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'id');
    }

    public function coordinator()
    {
        return $this->belongsTo(Teacher::class, 'coordinator_id', 'id');
    }

    public function deadlines(){
        return $this->hasMany(Deadline::class);
    }
}
