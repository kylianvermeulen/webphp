<?php

namespace App\Policies;

use App\Deadline;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeadlinePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any deadlines.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->type === 'deadline';
    }

    /**
     * Determine whether the user can view the deadline.
     *
     * @param  \App\User  $user
     * @param  \App\Deadline  $deadline
     * @return mixed
     */
    public function view(User $user, Deadline $deadline)
    {
        //
    }

    /**
     * Determine whether the user can create deadlines.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->type === 'deadline';
    }

    /**
     * Determine whether the user can update the deadline.
     *
     * @param  \App\User  $user
     * @param  \App\Deadline  $deadline
     * @return mixed
     */
    public function update(User $user, Deadline $deadline)
    {
        return $user->type === 'deadline';
    }

    /**
     * Determine whether the user can delete the deadline.
     *
     * @param  \App\User  $user
     * @param  \App\Deadline  $deadline
     * @return mixed
     */
    public function delete(User $user, Deadline $deadline)
    {
        return $user->type === 'deadline';
    }

    /**
     * Determine whether the user can restore the deadline.
     *
     * @param  \App\User  $user
     * @param  \App\Deadline  $deadline
     * @return mixed
     */
    public function restore(User $user, Deadline $deadline)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the deadline.
     *
     * @param  \App\User  $user
     * @param  \App\Deadline  $deadline
     * @return mixed
     */
    public function forceDelete(User $user, Deadline $deadline)
    {
        //
    }
}
