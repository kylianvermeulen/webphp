<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Deadline
 * @package App
 * @property int id
 * @property int module_id
 * @property string name
 * @property string deadline_at
 * @property boolean completed
 * @property int difficulty_id
 * @property int time_commitment_id
 * @property int fun_id
 * @property string created_at
 * @property string updated_at
 *
 * @property Module module
 * @property Tag difficulty
 * @property Tag time_commitment
 * @property Tag fun
 */
class Deadline extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'module_id',
        'name',
        'deadline_at',
        'difficulty_id',
        'time_commitment_id',
        'fun_id',
    ];

    protected $casts = [
        'completed' => 'boolean',
    ];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function difficulty()
    {
        return $this->belongsTo(Tag::class, 'difficulty_id', 'id');
    }

    public function time_commitment()
    {
        return $this->belongsTo(Tag::class, 'time_commitment_id', 'id');
    }

    public function fun()
    {
        return $this->belongsTo(Tag::class, 'fun_id', 'id');
    }
}
