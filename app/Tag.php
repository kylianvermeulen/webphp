<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App
 * @property int id
 * @property string name
 * @property string type
 * @property string created_at
 * @property string updated_at
 */
class Tag extends Model
{
    //
}
