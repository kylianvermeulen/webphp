<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Teacher
 * @package App
 * @property int id
 * @property string name
 * @property string created_at
 * @property string updated_at
 *
 * @property Module[] teacherModules
 * @property Module[] coordinatorModules
 */
class Teacher extends Model
{
    use Encryptable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $encryptable = [
        'name',
    ];

    public function teacherModules()
    {
        return $this->hasMany(Module::class, 'teacher_id', 'id');

    }

    public function coordinatorModules()
    {
        return $this->hasMany(Module::class, 'coordinator_id', 'id');
    }
}
