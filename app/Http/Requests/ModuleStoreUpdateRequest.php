<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModuleStoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'max:64'],
            'description' => ['required', 'max:64'],
            'period' => ['required', 'int', 'max:16'],
            'ec' => ['required', 'int', 'max:15'],
            'type' => ['required', 'string', 'max:64'],
            'file_path' => ['nullable', 'mimes:zip'],
            'teacher_id' => ['required', 'int','exists:teachers,id'],
            'coordinator_id' => ['required', 'int', 'exists:teachers,id'],
        ];
        if ($this->score != null) {
            $rules_score = ['score' => ['int']];
            $rules = array_merge($rules, $rules_score);
        }
        if ($this->completed != null) {
            $rules_completed = ['completed' => ['int', 'min:0', 'max:1']];
            $rules = array_merge($rules, $rules_completed);
        }
        return $rules;
    }
}
