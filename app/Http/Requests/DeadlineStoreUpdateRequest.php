<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class DeadlineStoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id' => ['required', 'int', 'exists:modules,id'],
            'name' => ['required', 'string', 'max:64'],
            'deadline_at' => ['required', 'date'],
            'difficulty_id' => ['required', 'int','exists:tags,id'],
            'time_commitment_id' => ['required', 'int','exists:tags,id'],
            'fun_id' => ['required', 'int','exists:tags,id'],
        ];
    }
}
