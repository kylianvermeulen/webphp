<?php

namespace App\Http\Controllers;

use App\Http\Requests\ModuleStoreUpdateRequest;
use App\Http\Requests\TeacherStoreUpdateRequest;
use App\Teacher;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Teacher::class);
        return view('teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ModuleStoreUpdateRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(TeacherStoreUpdateRequest $request)
    {
        $this->authorize('create', Teacher::class);
        $data = $request->validated();
        Teacher::create($data);
        return redirect()->route('admin', ['filter' => 'teachers']);
    }

    /**
     * Display the specified resource.
     *
     * @param Teacher $teacher
     * @return void
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Teacher $teacher
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(Teacher $teacher)
    {
        $this->authorize('update', $teacher);
        return view('teacher.edit')->with([
            'teacher' => $teacher,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TeacherStoreUpdateRequest $request
     * @param Teacher $teacher
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(TeacherStoreUpdateRequest $request, Teacher $teacher)
    {
        $this->authorize('update', $teacher);
        $data = $request->validated();
        $teacher->update($data);
        return redirect()->route('admin', ['filter' => 'teachers']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Teacher $teacher
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Teacher $teacher)
    {
        $this->authorize('delete', $teacher);
        Teacher::destroy($teacher->id);
        return redirect()->back();
    }
}
