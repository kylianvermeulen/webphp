<?php

namespace App\Http\Controllers;

use App\Deadline;
use App\Http\Requests\DeadlineStoreUpdateRequest;
use App\Module;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DeadlineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Deadline::class);
        $search = $request->search ?? '';

        $deadlineSort = $request->deadlineSort ?? 'none';
        $deadlineTimeSort = $request->deadlineTimeSort ?? 'none';
        $moduleSort = $request->moduleSort ?? 'none';
        $docentSort = $request->docentSort ?? 'none';
        $difficultySort = $request->difficultySort ?? 'none';
        $timeCommitmentSort = $request->timeCommitmentSort ?? 'none';
        $funSort = $request->funSort ?? 'none';

        $query = Deadline::query()->where('deadlines.name', 'like', '%'.$search.'%');

        if ($deadlineSort == 'asc' || $deadlineSort == 'desc') {
            $query->orderBy('deadlines.name', $deadlineSort);
        }
        if ($deadlineTimeSort == 'asc' || $deadlineTimeSort == 'desc') {
            $query->orderBy('deadlines.deadline_at', $deadlineTimeSort);
        }
        if ($moduleSort == 'asc' || $moduleSort == 'desc') {
            $query->rightJoin('modules', 'deadlines.module_id', '=', 'modules.id');
            $query->orderBy('modules.name', $moduleSort);
        }
        if ($docentSort == 'asc' || $docentSort == 'desc') {
            $query->rightJoin('modules', 'deadlines.module_id', '=', 'modules.id');
            $query->orderBy('modules.teacher_id', $docentSort);
        }
        if ($difficultySort == 'asc' || $difficultySort == 'desc') {
            $query->orderBy('difficulty_id', $difficultySort);
        }
        if ($timeCommitmentSort == 'asc' || $timeCommitmentSort == 'desc') {
            $query->orderBy('time_commitment_id', $timeCommitmentSort);
        }
        if ($funSort == 'asc' || $funSort == 'desc') {
            $query->orderBy('fun_id', $funSort);
        }

        $data = $query->orderBy('deadlines.updated_at', 'desc')->paginate();
        return view('deadline.index')->with([
            'data' => $data->items(),
            'search' => $search,
            'deadlineSort' => $deadlineSort,
            'deadlineTimeSort' => $deadlineTimeSort,
            'moduleSort' => $moduleSort,
            'docentSort' => $docentSort,
            'difficultySort' => $difficultySort,
            'timeCommitmentSort' => $timeCommitmentSort,
            'funSort' => $funSort,
            'tags' => Tag::all(),
            'totalCount' => $data->total(),
            'currentPage' => $data->currentPage(),
            'lastPage' => $data->lastPage(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Deadline::class);
        return view('deadline.create')->with([
            'modules' => Module::all(),
            'difficulty_tags' => Tag::where('type', '=', 'difficulty')->get(),
            'time_commitment_tags' => Tag::where('type', '=', 'time_commitment')->get(),
            'fun_tags' => Tag::where('type', '=', 'fun')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DeadlineStoreUpdateRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(DeadlineStoreUpdateRequest $request)
    {
        $this->authorize('create', Deadline::class);
        $data = $request->validated();
        Deadline::create($data);
        return redirect()->route('deadlines.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Deadline $deadline
     * @return void
     */
    public function show(Deadline $deadline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Deadline $deadline
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(Deadline $deadline)
    {
        $this->authorize('update', $deadline);
        return view('deadline.edit')->with([
            'deadline' => $deadline,
            'modules' => Module::all(),
            'difficulty_tags' => Tag::where('type', '=', 'difficulty')->get(),
            'time_commitment_tags' => Tag::where('type', '=', 'time_commitment')->get(),
            'fun_tags' => Tag::where('type', '=', 'fun')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DeadlineStoreUpdateRequest $request
     * @param Deadline $deadline
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(DeadlineStoreUpdateRequest $request, Deadline $deadline)
    {
        $this->authorize('update', $deadline);
        $data = $request->validated();
        $deadline->update($data);
        return redirect()->route('deadlines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Deadline $deadline
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Deadline $deadline)
    {
        $this->authorize('delete', $deadline);
        Deadline::destroy($deadline->id);
        return redirect()->back();
    }

    /**
     * @param Deadline $deadline
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function actionComplete(Deadline $deadline)
    {
        $this->authorize('update', $deadline);
        if ($deadline->completed) $deadline->completed = false;
        else $deadline->completed = true;
        $deadline->save();
        return redirect()->back();
    }
}
