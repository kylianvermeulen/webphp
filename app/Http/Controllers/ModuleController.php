<?php

namespace App\Http\Controllers;

use App\Http\Requests\ModuleStoreUpdateRequest;
use App\Module;
use App\Teacher;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Module::class);
        return view('module.create')->with([
            'types' => ['assessment', 'tentamen'],
            'teachers' => Teacher::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ModuleStoreUpdateRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(ModuleStoreUpdateRequest $request)
    {
        $this->authorize('create', Module::class);
        $data = $request->validated();
        if ($request->file_path) {
            $file_path = $request->file_path->store('files', 'public');
            $fileData = ['file_path' => $file_path];
            $data = array_merge($data, $fileData);
        }
        Module::create($data);
        return redirect()->route('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param Module $module
     * @return void
     */
    public function show(Module $module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Module $module
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(Module $module)
    {
        $this->authorize('update', $module);
        return view('module.edit')->with([
            'module' => $module,
            'types' => ['assessment', 'tentamen'],
            'teachers' => Teacher::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ModuleStoreUpdateRequest $request
     * @param Module $module
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(ModuleStoreUpdateRequest $request, Module $module)
    {
        $this->authorize('update', $module);
        $data = $request->validated();
        if ($request->file_path) {
            $file_path = $request->file_path->store('files', 'public');
            $fileData = ['file_path' => $file_path];
            $data = array_merge($data, $fileData);
        }
        $module->update($data);
        return redirect()->route('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Module $module
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Module $module)
    {
        $this->authorize('delete', $module);
        Module::destroy($module->id);
        return redirect()->back();
    }
}
