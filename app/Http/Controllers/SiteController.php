<?php

namespace App\Http\Controllers;

use App\Module;
use App\Teacher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['actionIndex']);
    }

    /**
     * @return Factory|View
     */
    public function actionIndex()
    {

        return view('site.index')->with([
            'module_ids' => Module::getModuleIds(),
            'semester_ids' => Module::getSemesterIds(),
            'modules' => Module::all(),
            'total_semesters' => count(Module::getSemesterIds())
        ]);
    }

    public function actionAdmin(Request $request)
    {
        if (auth()->user()->type != 'admin') return redirect()->back();
        $search = $request->search ?? '';
        $filter = $request->filter ?? 'modules';
        if ($filter === 'modules') {
            $data = Module::where('name', 'lIKE', '%' . $search . '%')->orderBy('updated_at', 'desc')->paginate();
        } elseif ($filter === 'teachers') {
            $data = Teacher::orderBy('updated_at', 'desc')->paginate();
        } else {
            return redirect()->back();
        }
        return view('site.admin')->with([
            'data' => $data->items(),
            'search' => $search,
            'filter' => $filter,
            'totalCount' => $data->total(),
            'currentPage' => $data->currentPage(),
            'lastPage' => $data->lastPage(),
        ]);
    }
}
